#!/bin/bash

############################## GENERAL PURPOSE ##############################

# Colors. Also present on many .bashrc, but not on every system!
alias ls='ls --color=auto'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'
alias grep='grep --color=auto'

# egrep, fgrep and rgrep are deprecated variants of the program grep. Alias it instead
alias egrep='grep -E'
alias fgrep='grep -F'
alias rgrep='grep -r'

# I don't like default ls aliases
alias ll='ls -lF'
alias la='ls -laF'
alias l='ls -CF'

# Use Git's colored diff when available
command -v git &>/dev/null && \
    alias dif='git diff --color=always --no-index --patch-with-stat'

# Omit empty lines and lines beggining by '\s*#', etc.
alias nocomment='sed -e '"'/^\s*\(#.*\)*$/d'"

# Start and stop an ssh-agent process
alias start-ssh-agent='eval "$(ssh-agent -s)"'
alias stop-ssh-agent='eval "$(ssh-agent -k)"'

# Lock terminal (pipes.sh: git clone https://github.com/pipeseroni/pipes.sh.git && cd pipes.sh && make install)
command -v pipes.sh &>/dev/null && command -v vlock &>/dev/null && \
    alias lock='/usr/local/bin/pipes.sh -r0 && vlock'

# IP addresses
alias myip="dig +short myip.opendns.com @resolver1.opendns.com"

# Intuitive map function
# For example, to list all directories that contain a certain file:
# find . -name .gitattributes | map dirname
alias map="xargs -n1"

# Print each PATH entry on a separate line
alias path='echo -e ${PATH//:/\\n}'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Try standardize some stuff between Linux, Windows (WSL) and OSX
# copy: copy content to clipboard (from stdin)
# open: open in file explorer
if [ "$(uname -s)" == 'Darwin' ]; then
    command -v pbcopy &>/dev/null && alias copy=pbcopy

elif [ -n "$WSLENV" ] && command -v clip.exe &>/dev/null; then
	alias copy=clip.exe
    alias open='explorer.exe';
else
	if command -v xsel &>/dev/null; then
		alias copy='xsel -ib'
	elif command -v xclip &>/dev/null; then
		alias copy='xclip -sel clip'
	fi
    alias open='xdg-open';
fi

############################## DOCKER ##############################

# Docker shortcuts
for c in docker podman; do
    if command -v "$c" &>/dev/null; then
        # dockerpsnet , podmanpsnet
        alias "$c"psnet="$c"' ps --format "table {{.ID}}\t{{.Image}}\t{{.RunningFor}}\t{{.Status}}\t{{.Networks}}\t{{.Ports}}\t{{.Names}}"'
        alias "$c"psvol="$c"' ps --format "table {{.ID}}\t{{.Image}}\t{{.Status}}\t{{.Size}}\t{{.Mounts}}\t{{.Names}}"'
    fi
done

############################## KUBERNETES ##############################

if command -v kubectl &>/dev/null; then
    alias k=kubectl
    alias kcon='kubectl config get-contexts'
    command -v __start_kubectl &>/dev/null && \
        complete -o default -F __start_kubectl k
fi
