#!/usr/bin/env bash

# Create a new directory and enter it
function mkd() {
	mkdir -p "$@" && cd "$_";
}

# `tre` is a shorthand for `tree` with hidden files and color enabled, ignoring
# the `.git` directory, listing directories first. The output gets piped into
# `less` with options to preserve color and line numbers, unless the output is
# small enough for one screen.
function tre() {
	tree -aC -I '.git|node_modules|__pycache__|.venv' --dirsfirst "$@" | less -FRNX;
}

# Start an HTTP server from a directory, optionally specifying the port
function server() {
	local port="${1:-9001}";
	sleep 1 && open "http://localhost:${port}/" &
	/usr/bin/env python3 -m http.server "$port" --bind 127.0.0.1
}

# Show cert for given host/url
showcert() {
    host_and_port="${1#https://}"
    host_and_port="${host_and_port#http://}"
    host_and_port="${host_and_port%%/*}"
    host="${host_and_port%%:*}"
    port="${host_and_port#*:}"
    [ "$port" = "$host" ] && port=443
    echo | openssl s_client -showcerts -servername "$host" -connect "$host:$port" 2>/dev/null | openssl x509 -inform pem -noout -text
}

# Show cert chain for given host/url
showchain() {
    local proto="${1%%://*}"
    local host_and_port="${1#$proto://}"
    host_and_port="${host_and_port%%/*}"
    local host="${host_and_port%%:*}"
    local port="${host_and_port#*:}"
    [ "$port" = "$host" ] && port=443

    local cert_data=""
    echo | openssl s_client -showcerts -servername "$host" -connect "$host:$port" 2>/dev/null | while IFS= read -r line; do
        cert_data+="$line"$'\n'
        if [[ "$line" == "-----END CERTIFICATE-----" ]]; then
            echo "$cert_data" | openssl x509 -noout -subject -issuer
            cert_data=""
        fi
    done
}

# Symmetric encrypt file(s) with gpg, no key cache
if command -v gpg &>/dev/null; then
    function encryptfile()
    {
        if [ $# -lt 1 ]; then
            printf "Usage:\n\tencryptfile FILE [FILE2 [FILE3 [...]]]\n" >&2
            return 1
        fi
        read -sp 'Enter passphrase: ' pass ; echo
        read -sp 'Confirm passphrase: ' pass_confirm ; echo
        if [ "$pass" != "$pass_confirm" ]; then
            echo "Error: passphrase does not match confirmation" >&2
            return 1
        fi
        for f in "$@"; do
            echo -n "Encrypting '$f'... " >&2
            printf '%s' "${pass}" | gpg --batch --yes \
                --symmetric --no-symkey-cache \
                --passphrase-fd 0 --cipher-algo AES256 \
                -o "$f".aes256 "$f"
            echo "Done"
        done
        pass=''
        unset pass
    }
    # Decrypt 1 file with no key cache
    alias decryptfile='gpg --decrypt --no-symkey-cache'
fi

# ssh-agent shared across sessions
# file location configured with SSH_STORED_AGENT_FILE, default ~/.ssh/agent
start_stored_ssh_agent() {
    # Idempotent: is the agent is already running, does nothing.
    # Pitfall: if an agent is running but wasn't started with this function,
    #          it will start a new one, but "stored".
    local agent_file="${SSH_STORED_AGENT_FILE:-"$HOME/.ssh/agent"}"

    # Try to source agent_file
    source "$agent_file" &>/dev/null

    # Check if the ssh-agent is still alive
    #   ret 0 = alive and has identities
    #   ret 1 = alive but has no identity
    #   ret 2 = unreachable agent
    ssh-add -l &>/dev/null
    if [ $? -eq 2 ]; then
        ssh-agent -s > "$agent_file"
        chmod 600 "$agent_file"
        source "$agent_file" &>/dev/null
    fi
}
