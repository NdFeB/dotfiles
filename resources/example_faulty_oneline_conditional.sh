#!/bin/bash

function ___f() {
  command -v ls && alias ll='ls -la'
  command -v this_does_not_exist && \
    alias t=this_does_not_exist
  # Without an explicit return, a function returns $?

  # If this function ends with a skipped `if`, it will return 0
  # if command -v this_does_not_exist; then
  #   alias t=this_does_not_exist
  # fi

  # A non-skipped `if` containing a failing command would return non-zero
  # if command -v this_does_not_exist; then
  #   command -v this_does_not_exist && \
  #     alias t=this_does_not_exist
  # fi
}

# The following conditional is bad, and will run ___f twice.
# DEBUG is unset, so ___f is called with redirect, but
# ___f returns non 0, so ___f is called again without redirect.
# This conditional syntax relies entirely on ___f return code
[ -z ${DEBUG+x} ] && ___f &>/dev/null || ___f

# A solution is to wrap the && into a subshell that always return true
[ -z ${DEBUG+x} ] && (___main &>/dev/null;:) || ___main

# Or to use if
if [ -z ${DEBUG+x} ]; then ___main &>/dev/null; else ___main; fi

# The first syntax
# [ -z ${DEBUG+x} ] && some_command && ___f &>/dev/null || ___f
# is good if the last command after || is to be executed if ANY
# of the previous commands was to fail (and not only the first condition).

unset ___f

# Why not using `exec &>/dev/null` ?

# exec &>/dev/null is fine for normal scripts. But sourced scripts
# (like .bashrc or similar) run in the same shell than the caller.
# So, the exec command also affects the caller shell, making it looking like hanging forever.
# This is pretty dangerous, as stdin would still be functional.
