#!/bin/bash

# This script installs all dotfiles for the current user.
# - If one of the target file is already file (most likely the case for .bashrc),
#   save it with a timestamp prefix then create our symlink.
# - If it's a symlink pointing to a file, just replace it
# - Else, fail (we don't want to destroy the environment)

ts="$(date +%Y%m%d_%H%M%S)"
this_dir="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

files='
.exports
.bash_completion
.bash_functions
.bash_aliases
.bash_prompt
.bash_profile
.bashrc

.inputrc
.vimrc
.gitconfig

.curlrc
.wgetrc

.screenrc
.tmux.conf

.starship.toml
'

for file in $files; do
    symlink_path="${HOME}/${file}"
    symlink_target="${this_dir}/${file}"
    
    symlink_path_is_file=
    skip=

    # Is symlink_path already a symlink?
    if [ -L "$symlink_path" ]; then
        # Not pointing to a file? Skip
        [ -f "$symlink_path" ] || skip="'$symlink_path' is a symlink, but does not point to a file, skipping"

        # If link is broken though, we override
        [ -e "$symlink_path" ] || skip=

    # Is symlink_path a file? Then save it
    elif [ -f "$symlink_path" ]; then
        symlink_path_is_file='y'

    # symlink_path exists but if neither file or symlink? skip
    elif [ -e "$symlink_path" ]; then
        skip="'$symlink_path' exists, but is neither a file or symlink, skipping"
    fi

    if [ -z "$skip" ]; then
        # Backup symlink_path if it's a real file
        [ -z "$symlink_path_is_file" ] || cp "$symlink_path" "${symlink_path}.${ts}"
        if [ $? -ne 0 ]; then
            echo "Error saving '$symlink_path' to '${symlink_path}.${ts}', skipping" >&2
        else
            # Create symlink 'symlink_path' pointing to 'symlink_target'
            ln -sf "$symlink_target" "$symlink_path"
        fi
    else
        echo "$skip" >&2
    fi
done

exit $?
